//creando servidor basico 
var express = require('express');
var app = express();

//rutas 
app.get('/', function(request, response){
	response.send('server on');
});

app.get('/ruta2', function(request, response){
	response.send('ruta2');
});

app.get('/ruta3', function(request, response){
	response.send('ruta3');
});

//se llama la app en el puerto 3000
app.listen(3000);