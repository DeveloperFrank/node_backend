var express = require('express');
var app = express();
var faker = require('faker'); //cargamos la libreria faker
var _= require('lodash');

app.get('/', function(request, response){
	response.send('Api on');
});

//creamos los datos random
function generarUser(){
	var randomId = faker.random.uuid();
	var randomName = faker.name.findName();
	var randomPhone = faker.phone.phoneNumberFormat();
	var randomEmail = faker.internet.email();
	var randomip = faker.internet.ip();

//se crea un user y se le asignan datos random
	return  {
		id: randomId,
		nombre: randomName,
		telefono: randomPhone,
		email: randomEmail,
		ip: randomip
	}
}

app.get('/api', function(request, response){
	var cantidad = _.random(5,5)
	var users = _.times(cantidad, generarUser)
	response.json(users);
});



app.listen(3000);